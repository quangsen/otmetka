<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'otmetka');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'ducromrom');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',U-tJ^H.imT( dD3JwR|~eS6DRbQ{twI^>QWF.%-GXhx8bn!P36d]+A|ARQa<RM%');
define('SECURE_AUTH_KEY',  'wG,eXcAC-|6IfDT6+/Cfu|~FY_EWF1-f#76`s?Y0@RQGh,r5 Gd$;yD0^@-xa>~3');
define('LOGGED_IN_KEY',    '#BQ+*xguCI--m!vW~@&>`-muzaI?aGB{U>8w`fH-Xo*Qd/(&o&U>Wp9.Ttq+/LmQ');
define('NONCE_KEY',        '.FWk%ga. m]T/Alf6I5x||Q*J4d %?o88rLN+_GL1qn{n<N(Ga+7+_v#>Re XUem');
define('AUTH_SALT',        'Qn`q(J+e_d|r@)?KNGj`!{,i=#$bgl+%ipA:Jf!uLL IRm=h5bbZF+A8bR~uld)|');
define('SECURE_AUTH_SALT', 'P6{<}E;Z--kTRMQczb8UM?^gB)|8!j*KOdugC|Q!EinkuyphU+l>q6-@8P:8hBUg');
define('LOGGED_IN_SALT',   ')5;:h7pd+l{-|74a3-=!,@EZZ|+Gp-Q`(pO3?+o^K}lnVuiW;%]+}ztT2,DQ{+$e');
define('NONCE_SALT',       ':&5a;n,o03YM+]+PEX(5^<Ehy7s$#bl-v@O}WKGzDn/)M=^|o1pZw[YnR]QCULK}');
define('FS_METHOD','direct');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ot_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
